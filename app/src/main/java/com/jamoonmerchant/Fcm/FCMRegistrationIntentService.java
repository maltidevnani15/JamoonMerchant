package com.jamoonmerchant.Fcm;


import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.Logger;
import com.jamoonmerchant.utils.Utils;

public class FCMRegistrationIntentService extends IntentService {

    public FCMRegistrationIntentService() {
        super("FCMRegistrationIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.storeString(this, Constant.FCM_UNIQUE_ID,refreshedToken);
        Log.e("token",refreshedToken);

    }
}
