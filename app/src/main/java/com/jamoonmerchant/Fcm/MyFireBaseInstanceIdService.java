package com.jamoonmerchant.Fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.Utils;

public class MyFireBaseInstanceIdService extends FirebaseInstanceIdService{
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.storeString(this, Constant.FCM_UNIQUE_ID,refreshedToken);
        Log.e("deviceToken",refreshedToken);
    }
}
