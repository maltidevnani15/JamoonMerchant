package com.jamoonmerchant.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jamoonmerchant.Models.BusinessCategoryListModel;
import com.jamoonmerchant.Models.LocationDataModel;
import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.ItemSpinnerBusinessCategoryBinding;
import com.jamoonmerchant.databinding.ItemSpinnerLocationBinding;


public class BusinessCategoryAdapter extends ArrayAdapter<BusinessCategoryListModel> {
    private LayoutInflater layoutInflater;
    public BusinessCategoryAdapter(Context context){
        super(context,0);
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        ItemSpinnerBusinessCategoryBinding itemSpinnerBusinessCategoryBinding;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_spinner_business_category, parent, false);
          itemSpinnerBusinessCategoryBinding = DataBindingUtil.bind(convertView);
            holder = new BusinessCategoryAdapter.ViewHolder(itemSpinnerBusinessCategoryBinding);
            convertView.setTag(holder);
        } else {
            holder = (BusinessCategoryAdapter.ViewHolder) convertView.getTag();
        }

        final BusinessCategoryListModel data = getItem(position);
        if (data != null) {
            holder.setItemSpinnerBusinessCategoryBinding(data);
        }
        return convertView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = layoutInflater.inflate(R.layout.item_spinner_business_category,null);
        }
        final TextView countryCode = (TextView) convertView.findViewById(R.id.item_spinner_tvv);
        final BusinessCategoryListModel businessCategoryListModel = getItem(position);
        countryCode.setText(businessCategoryListModel.getBusinessCategoryName());
        return convertView;
    }

    public class ViewHolder{
        ItemSpinnerBusinessCategoryBinding itemSpinnerBusinessCategoryBinding;
        public ViewHolder(ItemSpinnerBusinessCategoryBinding itemSpinnerBusinessCategoryBinding) {
            this.itemSpinnerBusinessCategoryBinding = itemSpinnerBusinessCategoryBinding;
        }
      public void setItemSpinnerBusinessCategoryBinding(BusinessCategoryListModel businessCategoryListModel){
          itemSpinnerBusinessCategoryBinding.setBusCat(businessCategoryListModel);
          itemSpinnerBusinessCategoryBinding.executePendingBindings();
      }
    }
}
