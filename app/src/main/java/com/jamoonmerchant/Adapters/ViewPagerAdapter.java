package com.jamoonmerchant.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragmentList = new ArrayList<>();
    private List<String> mfragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager, List<Fragment> list) {
        super(manager);
        this.fragmentList = list;
    }

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }



    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        fragmentList.add(fragment);
        mfragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mfragmentTitleList.get(position);
    }


}
