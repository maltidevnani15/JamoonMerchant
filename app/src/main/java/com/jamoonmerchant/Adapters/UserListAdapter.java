package com.jamoonmerchant.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamoonmerchant.Interfaces.OnRecyclerItemClickListener;
import com.jamoonmerchant.Models.UserListModel;
import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.RowFragmentUserListBinding;

import java.util.ArrayList;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<UserListModel> userListArrayListModel;
    private LayoutInflater layoutInflater;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    public UserListAdapter(Context context, ArrayList<UserListModel> userListArrayListModel) {
        this.context = context;
        this.userListArrayListModel = userListArrayListModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        RowFragmentUserListBinding binding = DataBindingUtil.inflate(layoutInflater,R.layout.row_fragment_user_list,parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(userListArrayListModel.get(position));
    }

    @Override
    public int getItemCount() {
        return userListArrayListModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RowFragmentUserListBinding binding;
        public ViewHolder(RowFragmentUserListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.rowFragmentUserListIvEdit.setOnClickListener(this);

        }
        public void bind(UserListModel userListModel){
            binding.setUserListModelData(userListModel);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View view) {
            if (onRecyclerItemClickListener != null)
                onRecyclerItemClickListener.onItemClick(getAdapterPosition(), view);
        }
    }
    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener){
        this.onRecyclerItemClickListener=onRecyclerItemClickListener;
    }

}
