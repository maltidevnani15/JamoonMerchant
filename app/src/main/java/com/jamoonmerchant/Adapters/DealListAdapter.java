package com.jamoonmerchant.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamoonmerchant.Interfaces.OnRecyclerItemClickListener;
import com.jamoonmerchant.Models.DealModel;
import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.RowDealsBinding;

import java.util.ArrayList;

public class DealListAdapter extends RecyclerView.Adapter<DealListAdapter.ViewHolder>  {
    private RowDealsBinding rowDealsBinding;
    private ArrayList<DealModel>dealModelArrayList;
    private Context context;
    private LayoutInflater inflater;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;
    public DealListAdapter(final Context context,final ArrayList<DealModel> dealModelArrayList){
        this.context=context;
        this.dealModelArrayList=dealModelArrayList;
    }


    @Override
    public int getItemCount() {
        return  dealModelArrayList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        rowDealsBinding = DataBindingUtil.inflate(inflater, R.layout.row_deals, parent, false);
        return new ViewHolder(rowDealsBinding);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(dealModelArrayList.get(position));


    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }




    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RowDealsBinding binding;
        public ViewHolder(RowDealsBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
            binding.rowDeelFeedAdapterCv.setOnClickListener(this);

        }
        public void bind(DealModel dealModel){
            binding.setDealData(dealModel);
            binding.executePendingBindings();
        }


        @Override
        public void onClick(View v) {
            if(onRecyclerItemClickListener!=null){
                onRecyclerItemClickListener.onItemClick(getAdapterPosition(),v);
            }
        }
    }
    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener){
        this.onRecyclerItemClickListener=onRecyclerItemClickListener;
    }
}
