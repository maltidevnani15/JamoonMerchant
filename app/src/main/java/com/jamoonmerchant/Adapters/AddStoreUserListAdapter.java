package com.jamoonmerchant.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.jamoonmerchant.Interfaces.OnItemCheckChangeListener;
import com.jamoonmerchant.Interfaces.OnRecyclerItemClickListener;
import com.jamoonmerchant.Models.UserListModel;
import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.RowFragmentAddStoreUserListBinding;


import java.util.ArrayList;

public class AddStoreUserListAdapter extends RecyclerView.Adapter<AddStoreUserListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<UserListModel> userListArrayListModel;
    private LayoutInflater layoutInflater;
    private OnItemCheckChangeListener onItemCheckChangeListener;


    public AddStoreUserListAdapter(Context context, ArrayList<UserListModel> userListArrayListModel, OnItemCheckChangeListener onItemCheckChangeListener) {
        this.context = context;
        this.userListArrayListModel = userListArrayListModel;
        this.onItemCheckChangeListener = onItemCheckChangeListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        RowFragmentAddStoreUserListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_fragment_add_store_user_list, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(userListArrayListModel.get(position));
        if(userListArrayListModel.get(position).isIfuserSelectedForAStore()){
            holder.binding.rowFragmentAddStoreUserListCb.setChecked(true);
        }


    }

    @Override
    public int getItemCount() {
        return userListArrayListModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {
        RowFragmentAddStoreUserListBinding binding;

        public ViewHolder(RowFragmentAddStoreUserListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.rowFragmentAddStoreUserListCb.setOnCheckedChangeListener(this);

        }

        public void bind(UserListModel userListModel) {
            binding.setUserListModelData(userListModel);
            binding.executePendingBindings();
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (onItemCheckChangeListener != null)
                onItemCheckChangeListener.onItemCheck(getAdapterPosition(), isChecked, itemView);
        }
    }

    public void setOnCheckItemListener(OnItemCheckChangeListener onItemCheckChangeListener) {
        this.onItemCheckChangeListener = onItemCheckChangeListener;
    }

}
