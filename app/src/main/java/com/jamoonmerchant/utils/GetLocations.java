package com.jamoonmerchant.utils;

import android.content.Context;

import com.jamoonmerchant.Interfaces.OnGetLocationListListener;
import com.jamoonmerchant.Models.ResponseOfAllApi;
import com.jamoonmerchant.webservices.RestClient;
import com.jamoonmerchant.webservices.RetrofitCallback;

import retrofit2.Call;

public class GetLocations {
    private Context context;
    private OnGetLocationListListener onGetLocationListListener;
    private Locations locations;
    public enum Locations{
        COUNTRY,STATE,CITY;
    }
    public GetLocations(Context context){
        this.context=context;
    }
    public void getCountryList(final Locations locations,final int languageId){
        this.locations=locations;
        Call<ResponseOfAllApi> getCountryList = RestClient.getInstance().getApiInterface().getLocation(languageId);
      getCountryList.enqueue(new RetrofitCallback<ResponseOfAllApi>(context,null) {
          @Override
          public void onSuccess(ResponseOfAllApi data) {
              if(!data.getLocationDataModelList().isEmpty()){
                  data.getLocationDataModelList().trimToSize();
                  if(onGetLocationListListener !=null){
                      onGetLocationListListener.onLocationGetList(data.getLocationDataModelList(),Locations.COUNTRY);
                  }
              }
          }
      });
    }
    public void getStateList(final Locations locations,final int languageId,final int countryId){
        this.locations=locations;
        Call<ResponseOfAllApi> getCountryList = RestClient.getInstance().getApiInterface().getState(languageId,countryId);
        getCountryList.enqueue(new RetrofitCallback<ResponseOfAllApi>(context,null) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if(!data.getStateDataList().isEmpty()){
                    data.getStateDataList().trimToSize();
                    if(onGetLocationListListener !=null){
                        onGetLocationListListener.onLocationGetList(data.getStateDataList(),Locations.STATE);
                    }
                }
            }
        });
    }
    public void getCityList(final Locations locations,final int languageId,final int countryId,final int stateId){
        this.locations=locations;
        Call<ResponseOfAllApi> getCountryList = RestClient.getInstance().getApiInterface().getCity(languageId,countryId,stateId);
        getCountryList.enqueue(new RetrofitCallback<ResponseOfAllApi>(context,null) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if(!data.getCityDataList().isEmpty()){
                    data.getCityDataList().trimToSize();
                    if(onGetLocationListListener !=null){
                        onGetLocationListListener.onLocationGetList(data.getCityDataList(),Locations.CITY);
                    }
                }
            }
        });
    }
    public void setListener(OnGetLocationListListener onGetLocationListListener){
        this.onGetLocationListListener=onGetLocationListListener;
    }
}
