package com.jamoonmerchant.Models;

import com.google.gson.annotations.SerializedName;

public class BusinessCategoryListModel {
    @SerializedName("BusinessCategoryId")
    private int businessCategoryId;
    @SerializedName("BusinessCategoryName")
    private String businessCategoryName;
    @SerializedName("LanguageID")
    private int languageId;

    public int getBusinessCategoryId() {
        return businessCategoryId;
    }

    public void setBusinessCategoryId(int businessCategoryId) {
        this.businessCategoryId = businessCategoryId;
    }

    public String getBusinessCategoryName() {
        return businessCategoryName;
    }

    public void setBusinessCategoryName(String businessCategoryName) {
        this.businessCategoryName = businessCategoryName;
    }

    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }
}
