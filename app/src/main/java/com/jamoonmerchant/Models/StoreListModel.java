package com.jamoonmerchant.Models;


import android.databinding.BaseObservable;
import android.databinding.Bindable;
import com.google.gson.annotations.SerializedName;


public class StoreListModel extends BaseObservable{
    @SerializedName("StoreId")
    private int storeId;
    @SerializedName("MerchantId")
    private int merchantId;
    @SerializedName("StoreName")
    private String storeName;
    @SerializedName("Address")
    private String address1;

    @SerializedName("AllowtoManageDeals")
    private int allowToManageDeal;
    @SerializedName("BusinessImage")
    private String businessImage;

    @SerializedName("IsActive")
    private int isActive;

    public boolean isStoreSelected() {
        return isStoreSelected;
    }

    public void setStoreSelected(boolean storeSelected) {
        isStoreSelected = storeSelected;
    }

    private boolean isStoreSelected;

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }
    @Bindable
    public String getBusinessImage() {
        return businessImage;
    }

    public void setBusinessImage(String businessImage) {
        this.businessImage = businessImage;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(int merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public int getAllowToManageDeal() {
        return allowToManageDeal;
    }

    public void setAllowToManageDeal(int allowToManageDeal) {
        this.allowToManageDeal = allowToManageDeal;
    }


}
