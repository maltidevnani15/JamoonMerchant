package com.jamoonmerchant.Models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

public class MerchantDetailModel extends BaseObservable {
    @SerializedName("MerchantType")
    private String merchantType;
    @SerializedName("MerchantId")
    private int merchantId;
    @SerializedName("FirstName")
    private String merchantUserFirstName;
    @SerializedName("LastName")
    private String merchantUserlastName;

    @SerializedName("PhoneNo")
    private String merchantUserPhoneNo;
    @SerializedName("MerchantUserIsActive")
    private int merchantUserIsActive;
    @SerializedName("AllowToManageDeals")
    private int merchantUserAllowToManageDeal;
    @SerializedName("EmailAddress")
    private String email;
    @SerializedName("MerchantUserId")
    private int merchantUserId;
    @SerializedName("Password")
    private String password;
    @SerializedName("BusinessImage")
    private String storeImage;
    public String getPassword() {
        return password;
    }
    @Bindable
    public String getStoreImage() {
        return storeImage;
    }

    public void setStoreImage(String storeImage) {
        this.storeImage = storeImage;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getMerchantUserFirstName() {
        return merchantUserFirstName;
    }

    public void setMerchantUserFirstName(String merchantUserFirstName) {
        this.merchantUserFirstName = merchantUserFirstName;
    }

    public String getMerchantUserlastName() {
        return merchantUserlastName;
    }

    public void setMerchantUserlastName(String merchantUserlastName) {
        this.merchantUserlastName = merchantUserlastName;
    }


    public String getMerchantUserPhoneNo() {
        return merchantUserPhoneNo;
    }

    public void setMerchantUserPhoneNo(String merchantUserPhoneNo) {
        this.merchantUserPhoneNo = merchantUserPhoneNo;
    }

    public int getMerchantUserIsActive() {
        return merchantUserIsActive;
    }

    public void setMerchantUserIsActive(int merchantUserIsActive) {
        this.merchantUserIsActive = merchantUserIsActive;
    }

    public int getMerchantUserAllowToManageDeal() {
        return merchantUserAllowToManageDeal;
    }

    public void setMerchantUserAllowToManageDeal(int merchantUserAllowToManageDeal) {
        this.merchantUserAllowToManageDeal = merchantUserAllowToManageDeal;
    }



    public int getMerchantUserId() {
        return merchantUserId;
    }

    public void setMerchantUserId(int merchantUserId) {
        this.merchantUserId = merchantUserId;
    }



    public String getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(int merchantId) {
        this.merchantId = merchantId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
