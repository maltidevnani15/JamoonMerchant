package com.jamoonmerchant.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResponseOfAllApi {

    @SerializedName("UserDetails")
    private MerchantDetailModel merchantDetailModel;
    @SerializedName("CountryList")
    private ArrayList<LocationDataModel> locationDataModelList;
    @SerializedName("StateList")
    private ArrayList<LocationDataModel> stateDataList;
    @SerializedName("BusinessCategoryList")
    private ArrayList<BusinessCategoryListModel> businessCategoryListArrayListModel;
    @SerializedName("MerchantUserList")
    private ArrayList<UserListModel> merchantUserListModel;
    @SerializedName("StoreList")
    private ArrayList<StoreListModel> storeListModelArrayList;
    @SerializedName("MerchantUserDetail")
    private MerchantDetailModel merchantUpdateUserData;
    @SerializedName("DealList")
    private ArrayList<DealModel>dealModelArrayList;
    @SerializedName("store")
    private StoreDeatilModel storeDeatilModel;
    @SerializedName("DealDetail")
    private DealModel dealModel;
    @SerializedName("CityList")
    private ArrayList<LocationDataModel>cityDataList;

    public DealModel getDealModel() {
        return dealModel;
    }

    public void setDealModel(DealModel dealModel) {
        this.dealModel = dealModel;
    }

    public StoreDeatilModel getStoreDeatilModel() {
        return storeDeatilModel;
    }

    public void setStoreDeatilModel(StoreDeatilModel storeDeatilModel) {
        this.storeDeatilModel = storeDeatilModel;
    }

    public ArrayList<DealModel> getDealModelArrayList() {
        return dealModelArrayList;
    }

    public void setDealModelArrayList(ArrayList<DealModel> dealModelArrayList) {
        this.dealModelArrayList = dealModelArrayList;
    }

    public MerchantDetailModel getMerchantUpdateUserData() {
        return merchantUpdateUserData;
    }

    public void setMerchantUpdateUserData(MerchantDetailModel merchantUpdateUserData) {
        this.merchantUpdateUserData = merchantUpdateUserData;
    }



    public ArrayList<StoreListModel> getStoreListModelArrayList() {
        return storeListModelArrayList;
    }

    public void setStoreListModelArrayList(ArrayList<StoreListModel> storeListModelArrayList) {
        this.storeListModelArrayList = storeListModelArrayList;
    }

    public ArrayList<UserListModel> getMerchantUserListModel() {
        return merchantUserListModel;
    }

    public void setMerchantUserListModel(ArrayList<UserListModel> merchantUserListModel) {
        this.merchantUserListModel = merchantUserListModel;
    }

    public ArrayList<BusinessCategoryListModel> getBusinessCategoryListArrayListModel() {
        return businessCategoryListArrayListModel;
    }

    public void setBusinessCategoryListArrayListModel(ArrayList<BusinessCategoryListModel> businessCategoryListArrayListModel) {
        this.businessCategoryListArrayListModel = businessCategoryListArrayListModel;
    }

    public ArrayList<LocationDataModel> getStateDataList() {
        return stateDataList;
    }

    public void setStateDataList(ArrayList<LocationDataModel> stateDataList) {
        this.stateDataList = stateDataList;
    }

    public ArrayList<LocationDataModel> getCityDataList() {
        return cityDataList;
    }

    public void setCityDataList(ArrayList<LocationDataModel> cityDataList) {
        this.cityDataList = cityDataList;
    }


    public ArrayList<LocationDataModel> getLocationDataModelList() {
        return locationDataModelList;
    }

    public void setLocationDataModelList(ArrayList<LocationDataModel> locationDataModelList) {
        this.locationDataModelList = locationDataModelList;
    }

    public MerchantDetailModel getMerchantDetailModel() {
        return merchantDetailModel;
    }

    public void setMerchantDetailModel(MerchantDetailModel merchantDetailModel) {
        this.merchantDetailModel = merchantDetailModel;
    }

}
