package com.jamoonmerchant.Interfaces;


import com.jamoonmerchant.Models.LocationDataModel;
import com.jamoonmerchant.utils.GetLocations;

import java.util.ArrayList;

public interface OnGetLocationListListener {
    public void onLocationGetList(ArrayList<LocationDataModel> locationDataModelArrayList, GetLocations.Locations locations);
}
