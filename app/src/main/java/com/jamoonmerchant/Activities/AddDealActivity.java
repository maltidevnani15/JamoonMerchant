package com.jamoonmerchant.Activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jamoonmerchant.Adapters.AddDealStoreListAdapter;
import com.jamoonmerchant.Interfaces.OnItemCheckChangeListener;
import com.jamoonmerchant.Models.DealModel;
import com.jamoonmerchant.Models.MerchantDetailModel;
import com.jamoonmerchant.Models.ResponseOfAllApi;
import com.jamoonmerchant.Models.StoreListModel;
import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.ActivityAddDealBinding;
import com.jamoonmerchant.imagepicker.ImagePicker;
import com.jamoonmerchant.imagepicker.ImagePickerInterface;
import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.Logger;
import com.jamoonmerchant.utils.Utils;
import com.jamoonmerchant.webservices.RestClient;
import com.jamoonmerchant.webservices.RetrofitCallback;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class AddDealActivity extends BaseActivityWithoutResideMenu implements ImagePickerInterface, OnItemCheckChangeListener, View.OnClickListener, ImagePicker.OnGetBitmapListener, TimePickerDialog.OnTimeSetListener, RadioGroup.OnCheckedChangeListener {
    private ActivityAddDealBinding activityAddDealBinding;
    private ImagePicker imagePicker;
    private Uri uri = null;
    private String tag;
    private String path1, path2, path3;
    private Date date;
    private Calendar dateTimeCalendar;
    private Calendar todayCalendar;
    private SimpleDateFormat simpleDateFormat;
    private SimpleDateFormat exclusiveDateFormat;

    private AddDealStoreListAdapter storeListAdapter;
    private ArrayList<StoreListModel> storeListModelArrayList;
    private ArrayList<StoreListModel> assignUserListModel;
    private StringBuffer stringBuffer;
    private int dealId;
    private List<String> fromUpdateDealSelectedStoreListId;
    private MultipartBody.Part imageOne;
    private MultipartBody.Part imageTwo;
    private MultipartBody.Part imageThree;
    private RequestBody DealName;
    private RequestBody DealActualPrice;
    private RequestBody DealDiscountPrice;
    private RequestBody DealDescription;
    private RequestBody StartDate;
    private RequestBody EndDate;
    private RequestBody storeIds;
    private RequestBody MerchantId;
    private RequestBody MerchantType;
    private RequestBody MerchantUserId;
    private RequestBody DealId;
    private RequestBody DealType;
    private RequestBody DealImageOneName;
    private RequestBody DealImageTwoName;
    private RequestBody DealImageThreeName;
    private RequestBody DealImageOneStatus;
    private RequestBody DealImageTwoStatus;
    private RequestBody DealImageThreeStatus;
    private String imageOneName;
    private String imageTwoName;
    private String imageThreeName;
    private int imageOneStatus=-1;
    private int imageTwoStatus=-1;
    private int imageThreeStatus=-1;
    private int hotDealTime;
    private int hotDealHr;
    private int hotDealMinute;
    private int todaysHr;
    private int dealType;

    @Override
    protected void initView() {
        dealId=getIntent().getIntExtra("dealId",0);
        activityAddDealBinding= DataBindingUtil.setContentView(this,R.layout.activity_add_deal);
        storeListModelArrayList = new ArrayList<>();
        assignUserListModel = new ArrayList<>();
        stringBuffer = new StringBuffer();
        date = new Date();
        dateTimeCalendar = Calendar.getInstance();
        todayCalendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat(Constant.EXPECTED_DATE_FORMATTER, Locale.getDefault());
        exclusiveDateFormat=new SimpleDateFormat(Constant.EXCLUSIVE_DATE_FORMATTER,Locale.getDefault());

        imagePicker = new ImagePicker(this,this);
        activityAddDealBinding.activityAddDealRv.setLayoutManager(new LinearLayoutManager(this));
        storeListAdapter = new AddDealStoreListAdapter(this, storeListModelArrayList);
        storeListAdapter.setOnCheckItemListener(this);
        activityAddDealBinding.activityAddDealRv.setAdapter(storeListAdapter);

        activityAddDealBinding.activityAddDealTvStartDate.setText(simpleDateFormat.format(date));

        activityAddDealBinding.activityAddDealRgDealType.setOnCheckedChangeListener(this);
        activityAddDealBinding.activityAddDealRgDuration.setOnCheckedChangeListener(this);
        activityAddDealBinding.activityAddDealTvStartTime.setOnClickListener(this);
        activityAddDealBinding.activityAddDealIv1.setOnClickListener(this);
        activityAddDealBinding.activityAddDealIv2.setOnClickListener(this);
        activityAddDealBinding.activityAddDealIv3.setOnClickListener(this);
        activityAddDealBinding.activityAddDealIv1Delete.setOnClickListener(this);
        activityAddDealBinding.activityAddDealIv2Delete.setOnClickListener(this);
        activityAddDealBinding.activityAddDealIv3Delete.setOnClickListener(this);
        activityAddDealBinding.activityAddDealTvStartDate.setOnClickListener(this);
        activityAddDealBinding.activityAddDealTvEndDate.setOnClickListener(this);
        activityAddDealBinding.activityAddDealTvPostDeal.setOnClickListener(this);
        imagePicker.setOnGetBitmapListener(this);
        getStoreList();


    }
    private void getDealById() {
        Call<ResponseOfAllApi> getDealById = RestClient.getInstance().getApiInterface().getDealById(dealId);
        getDealById.enqueue(new RetrofitCallback<ResponseOfAllApi>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if (data.getDealModel() != null) {
                    activityAddDealBinding.setDealData(data.getDealModel());
                    activityAddDealBinding.executePendingBindings();
                    setEditData(data.getDealModel());
                }
            }
            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    private void setEditData(DealModel dealModel) {
        if(!TextUtils.isEmpty(dealModel.getDealImageOne())){
            imageOneName=dealModel.getDealImageOneName();
            imageOneStatus=0;

        }
        if(!TextUtils.isEmpty(dealModel.getDealImageTwo())){
            imageTwoName=dealModel.getDealImageTwoName();
            imageTwoStatus=0;

        }
        if(!TextUtils.isEmpty(dealModel.getDealImageThree())){
            imageThreeName=dealModel.getDealImageThreeName();
            imageThreeStatus=0;

        }

        if(!TextUtils.isEmpty(dealModel.getDealImageOne())){
            activityAddDealBinding.activityAddDealIv1Delete.setVisibility(View.VISIBLE);
        }else{
            activityAddDealBinding.activityAddDealIv1Delete.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(dealModel.getDealImageTwo())){
            activityAddDealBinding.activityAddDealIv2Delete.setVisibility(View.VISIBLE);
        }else{
            activityAddDealBinding.activityAddDealIv2Delete.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(dealModel.getDealImageThree())){
            activityAddDealBinding.activityAddDealIv3Delete.setVisibility(View.VISIBLE);
        }else{
            activityAddDealBinding.activityAddDealIv3Delete.setVisibility(View.GONE);
        }
        String str = dealModel.getStoreIds();
        fromUpdateDealSelectedStoreListId = Arrays.asList(str.split(","));
        for (int i = 0; i < storeListModelArrayList.size(); i++) {
            if (fromUpdateDealSelectedStoreListId.contains(String.valueOf(storeListModelArrayList.get(i).getStoreId()))) {
                storeListModelArrayList.get(i).setStoreSelected(true);
                storeListAdapter.notifyDataSetChanged();

            }
        }
    }
    private void getStoreList() {
        storeListModelArrayList.clear();
        int merchantId = 0;
        String userData = Utils.getString(this, Constant.USER_DATA);
        if (userData != null) {
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData, MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
        }
        Call<ResponseOfAllApi> storeListCall = RestClient.getInstance().getApiInterface().getStoreList(merchantId);
        storeListCall.enqueue(new RetrofitCallback<ResponseOfAllApi>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if (!data.getStoreListModelArrayList().isEmpty()) {
                    storeListModelArrayList.trimToSize();
                    storeListModelArrayList.addAll(data.getStoreListModelArrayList());
                    storeListAdapter.notifyDataSetChanged();
                    if (dealId != 0) {
                        getDealById();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
                Logger.toast(AddDealActivity.this,"Kindly add a user and a store ");
//                onBackPressed();
            }
        });
    }
    @Override
    protected void initToolBar() {
        final Toolbar toolbar = (Toolbar)findViewById(R.id.activity_add_deal_tb);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        showBackButton(R.drawable.ic_back);
    }


    @Override
    public void handleCamera(Intent takePictureIntent) {
        uri = Uri.fromFile(new File(imagePicker.createOrGetProfileImageDir(this), System.currentTimeMillis() + ".jpg"));
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(takePictureIntent, ImagePicker.CAMERA_REQUEST);
    }

    @Override
    public void handleGallery(Intent galleryPickerIntent) {
        startActivityForResult(galleryPickerIntent, ImagePicker.GALLERY_REQUEST);
    }

    @Override
    public void onItemCheck(int position, boolean is_checked, View view) {
        if (is_checked) {
            assignUserListModel.add(storeListModelArrayList.get(position));
        } else {
            assignUserListModel.remove(storeListModelArrayList.get(position));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_add_deal_tv_post_deal:
                Utils.hideSoftKeyboard(this);
                validateData();
                break;
            case R.id.activity_add_deal_iv1:
                tag = "Iv1";
                openImagePicker(v);
                break;
            case R.id.activity_add_deal_iv2:
                tag = "Iv2";
                openImagePicker(v);
                break;
            case R.id.activity_add_deal_iv3:
                tag = "Iv3";
                openImagePicker(v);
                break;
            case R.id.activity_add_deal_iv1_delete:
                path1 = "";
                tag = "";
                imageOneStatus = -1;

                activityAddDealBinding.activityAddDealIv1.setImageDrawable(getResources().getDrawable(R.drawable.camera));
                activityAddDealBinding.activityAddDealIv1Delete.setVisibility(View.GONE);
                break;
            case R.id.activity_add_deal_iv2_delete:
                activityAddDealBinding.activityAddDealIv2.setImageDrawable(getResources().getDrawable(R.drawable.camera));
                activityAddDealBinding.activityAddDealIv2Delete.setVisibility(View.GONE);
                path2 = "";
                tag = "";
                imageTwoStatus = -1;

                break;
            case R.id.activity_add_deal_iv3_delete:
                activityAddDealBinding.activityAddDealIv3.setImageDrawable(getResources().getDrawable(R.drawable.camera));
                activityAddDealBinding.activityAddDealIv3Delete.setVisibility(View.GONE);
                path3 = "";
                tag = "";
                imageThreeStatus = -1;
                break;
            case R.id.activity_add_deal_tv_start_date:
                new DatePickerDialog(this, onStartDateListener, dateTimeCalendar.get(Calendar.YEAR),
                        dateTimeCalendar.get(Calendar.MONTH), dateTimeCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.activity_add_deal_tv_end_date:
                new DatePickerDialog(this, onEndDateListener, dateTimeCalendar.get(Calendar.YEAR),
                        dateTimeCalendar.get(Calendar.MONTH), dateTimeCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.activity_add_deal_tv_start_time:
                todaysHr = todayCalendar.get(Calendar.HOUR_OF_DAY);
                int minute = todayCalendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(this, this, todaysHr, minute, true);
                timePickerDialog.setTitle("Select Deal Time");
                timePickerDialog.show();
                break;
        }
    }


    private void validateData() {
        setSelectedStoreList();
        final String dealname = activityAddDealBinding.activityAddDealEtDealName.getText().toString();
        final String dealActualPrice = activityAddDealBinding.activityAddDealEtActualPrice.getText().toString();
        final String dealDiscountPrice = activityAddDealBinding.activityAddDealEtDiscountPrice.getText().toString();
        final String dealDescription = activityAddDealBinding.activityAddDealEtDealDescription.getText().toString();
        final String startDate = activityAddDealBinding.activityAddDealTvStartDate.getText().toString();

        if(dealId!=0){
            if( imageOneStatus!=-1 || imageTwoStatus!=-1||imageThreeStatus!=-1){
                validateOtherData(dealname,dealActualPrice,dealDiscountPrice,dealDescription,startDate);
            }else{
                Logger.showSnackbar(AddDealActivity.this, "Add atleast one image");
            }
        }else{
            if (!TextUtils.isEmpty(path1) || !TextUtils.isEmpty(path2) || !TextUtils.isEmpty(path3)) {
                validateOtherData(dealname,dealActualPrice,dealDiscountPrice,dealDescription,startDate);
            } else {
                Logger.showSnackbar(AddDealActivity.this, "Add atleast one image");
            }
        }
    }

    private void validateOtherData(String dealname, String dealActualPrice, String dealDiscountPrice, String dealDescription, String startDate) {
        if (!TextUtils.isEmpty(dealname)) {
            if (!TextUtils.isEmpty(dealActualPrice)) {
                if (!TextUtils.isEmpty(dealDiscountPrice)) {
                    if (!TextUtils.isEmpty(dealDescription)) {
                        if (!TextUtils.isEmpty(startDate)) {
                                if (stringBuffer.length() != 0) {
                                    if (dealId != 0) {
                                        if(activityAddDealBinding.activityAddDealRbHotDeal.isChecked()){
                                            if(!TextUtils.isEmpty(activityAddDealBinding.activityAddDealTvStartTime.getText())){
                                                updateDeal(dealname,  dealDiscountPrice, dealActualPrice,dealDescription, startDate, activityAddDealBinding.activityAddDealTvStartTime.getText().toString());
                                            }else{
                                                Logger.showSnackbar(this,"Select Start Time");
                                            }
                                        }else{
                                            if(!TextUtils.isEmpty(activityAddDealBinding.activityAddDealTvEndDate.getText())){
                                                updateDeal(dealname,  dealDiscountPrice, dealActualPrice,dealDescription, startDate, activityAddDealBinding.activityAddDealTvEndDate.getText().toString());
                                            }else{
                                                Logger.showSnackbar(this,"Select End Date");
                                            }
                                        }

                                    } else {
                                        if(activityAddDealBinding.activityAddDealRbHotDeal.isChecked()){
                                            if(!TextUtils.isEmpty(activityAddDealBinding.activityAddDealTvStartTime.getText())){
                                                insertDeal(dealname,  dealDiscountPrice, dealActualPrice,dealDescription, startDate, activityAddDealBinding.activityAddDealTvStartTime.getText().toString());
                                            }else{
                                                Logger.showSnackbar(this,"Select Start Time");
                                            }
                                        }else{
                                            if(!TextUtils.isEmpty(activityAddDealBinding.activityAddDealTvEndDate.getText())){
                                                insertDeal(dealname,  dealDiscountPrice, dealActualPrice,dealDescription, startDate, activityAddDealBinding.activityAddDealTvEndDate.getText().toString());
                                            }else{
                                                Logger.showSnackbar(this,"Select End Date");
                                            }
                                        }
                                    }
                                } else {
                                    Logger.showSnackbar(AddDealActivity.this, "Select atleast one store");
                                }

                        } else {
                            Logger.showSnackbar(AddDealActivity.this, "Select start date");
                        }
                    } else {
                        Logger.showSnackbar(AddDealActivity.this, "Enter deal description");
                    }
                } else {
                    Logger.showSnackbar(AddDealActivity.this, "Enter deal discount price");
                }
            } else {
                Logger.showSnackbar(AddDealActivity.this, "Enter deal actual price");
            }
        } else {
            Logger.showSnackbar(AddDealActivity.this, "Enter deal name");
        }
    }


    private void updateDeal(String dealname, String dealActualPrice, String dealDiscountPrice, String dealDescription, String startDate, String endDate) {
        setRequestBody(dealname, dealActualPrice, dealDiscountPrice, dealDescription, startDate, endDate);
        Call<JsonObject> updateDeal = RestClient.getInstance().getApiInterface().updateDeal(DealName, DealActualPrice, DealDiscountPrice, storeIds, StartDate, EndDate, MerchantType, MerchantId, MerchantUserId, DealDescription,
                DealId,DealImageOneName,DealImageTwoName,DealImageThreeName,DealImageOneStatus,DealImageTwoStatus,DealImageThreeStatus, imageOne, imageTwo, imageThree);
        updateDeal.enqueue(new RetrofitCallback<JsonObject>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if (data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")) {
                    Logger.toast(AddDealActivity.this, data.get(Constant.KEY_MESSAGE).getAsString());
                   onBackPressed();
                }else{
                    Logger.toast(AddDealActivity.this, data.get(Constant.KEY_MESSAGE).getAsString());
                   onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    private void setSelectedStoreList() {
        if(!assignUserListModel.isEmpty()){
            for (int i = 0; i < assignUserListModel.size(); i++) {
                stringBuffer.append(assignUserListModel.get(i).getStoreId());
                stringBuffer.append(",");
            }
            stringBuffer.setLength(stringBuffer.length() - 1);
        }
    }

    private void insertDeal(String dealname, String dealActualPrice, String dealDiscountPrice, String dealDescription, String startDate, String endDate) {
        setRequestBody(dealname, dealActualPrice, dealDiscountPrice, dealDescription, startDate, endDate);
        Call<JsonObject> insertDeal = RestClient.getInstance().getApiInterface().postDeal(DealName,DealDiscountPrice, DealActualPrice, storeIds, StartDate, EndDate, MerchantType, MerchantId, MerchantUserId, DealDescription,
                DealType,imageOne, imageTwo, imageThree);
        insertDeal.enqueue(new RetrofitCallback<JsonObject>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if (data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")) {
                    Logger.toast(AddDealActivity.this, data.get(Constant.KEY_MESSAGE).getAsString());

                }
              onBackPressed();
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
               onBackPressed();
            }
        });
    }

    private void setRequestBody(String dealname, String dealActualPrice, String dealDiscountPrice, String dealDescription, String startDate, String endDate) {
        int merchantId = 0;
        int merchantUserId = 0;
        int merchantType = Integer.parseInt(Utils.getString(this,Constant.USERTYPE));
        String userData = Utils.getString(this, Constant.USER_DATA);
        if (userData != null) {
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData, MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
            merchantUserId = merchantDetailModel.getMerchantUserId();
        }
        final File file1;
        final File file2;
        final File file3;

        final String mediaType = "text/plain";
        if (!TextUtils.isEmpty(path1)) {
            file1 = new File(path1);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file1);
            imageOne = MultipartBody.Part.createFormData("file0", file1.getName(), reqFile);
        } else {
            imageOne = MultipartBody.Part.createFormData("file0", " ");
        }
        if (!TextUtils.isEmpty(path2)) {
            file2 = new File(path2);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file2);
            imageTwo = MultipartBody.Part.createFormData("file1", file2.getName(), reqFile);
        } else {
            imageTwo = MultipartBody.Part.createFormData("file1", " ");
        }
        if (!TextUtils.isEmpty(path3)) {
            file3 = new File(path3);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file3);
            imageThree = MultipartBody.Part.createFormData("file2", file3.getName(), reqFile);
        } else {
            imageThree = MultipartBody.Part.createFormData("file2", " ");
        }
        DealName = RequestBody.create(MediaType.parse(mediaType), dealname);
        DealActualPrice = RequestBody.create(MediaType.parse(mediaType), dealActualPrice);
        DealDiscountPrice = RequestBody.create(MediaType.parse(mediaType), dealDiscountPrice);
        DealDescription = RequestBody.create(MediaType.parse(mediaType), dealDescription);

        storeIds = RequestBody.create(MediaType.parse(mediaType), stringBuffer.toString());
        MerchantId = RequestBody.create(MediaType.parse(mediaType), String.valueOf(merchantId));
        MerchantType = RequestBody.create(MediaType.parse(mediaType), String.valueOf(merchantType));
        MerchantUserId = RequestBody.create(MediaType.parse(mediaType), String.valueOf(merchantUserId));
        DealId = RequestBody.create(MediaType.parse(mediaType), String.valueOf(dealId));
        DealType=RequestBody.create(MediaType.parse(mediaType),String.valueOf(dealType));
        if(dealId!=0){
            if(!TextUtils.isEmpty(imageOneName)){
                DealImageOneName=RequestBody.create(MediaType.parse(mediaType),imageOneName);
                DealImageOneStatus=RequestBody.create(MediaType.parse(mediaType),String.valueOf(imageOneStatus));
            }else {
                DealImageOneName=RequestBody.create(MediaType.parse(mediaType)," ");
            }if(!TextUtils.isEmpty(imageTwoName)){
                DealImageTwoName= RequestBody.create(MediaType.parse(mediaType),imageTwoName);
                DealImageTwoStatus=RequestBody.create(MediaType.parse(mediaType),String.valueOf(imageTwoStatus));
            }else{
                DealImageTwoName= RequestBody.create(MediaType.parse(mediaType),"");
                DealImageTwoStatus=RequestBody.create(MediaType.parse(mediaType),"");
            }if(!TextUtils.isEmpty(imageThreeName)){
                DealImageThreeName=RequestBody.create(MediaType.parse(mediaType),imageThreeName);
                DealImageThreeStatus=RequestBody.create(MediaType.parse(mediaType),String.valueOf(imageThreeStatus));
            }else{
                DealImageThreeName=RequestBody.create(MediaType.parse(mediaType),"");
                DealImageThreeStatus=RequestBody.create(MediaType.parse(mediaType),"");
            }
        }
        if(activityAddDealBinding.activityAddDealHotDealView.getVisibility()==View.VISIBLE){

            date.setHours(hotDealHr);
            date.setMinutes(hotDealMinute);
            final Date formatedEndDate=new Date();
            int endhr=hotDealHr+hotDealTime;
            formatedEndDate.setHours(endhr);
            formatedEndDate.setMinutes(hotDealMinute);
            StartDate = RequestBody.create(MediaType.parse(mediaType), String.valueOf(simpleDateFormat.format(date)));

            EndDate = RequestBody.create(MediaType.parse(mediaType), String.valueOf(simpleDateFormat.format(formatedEndDate)));
        }else{
            StartDate = RequestBody.create(MediaType.parse(mediaType), startDate);
            EndDate = RequestBody.create(MediaType.parse(mediaType), endDate );
        }
    }

    private DatePickerDialog.OnDateSetListener onStartDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeCalendar.set(Calendar.YEAR, year);
            dateTimeCalendar.set(Calendar.MONTH, month);
            dateTimeCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (dateTimeCalendar.compareTo(todayCalendar) < 0) {
                Logger.showSnackbar(AddDealActivity.this, getString(R.string.select_more_than_todays_date));
            } else {
                activityAddDealBinding.activityAddDealTvStartDate.setText(exclusiveDateFormat.format(dateTimeCalendar.getTime()));
            }
        }
    };
    private DatePickerDialog.OnDateSetListener onEndDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeCalendar.set(Calendar.YEAR, year);
            dateTimeCalendar.set(Calendar.MONTH, month);
            dateTimeCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (dateTimeCalendar.compareTo(todayCalendar) < 0) {
                Logger.showSnackbar(AddDealActivity.this, getString(R.string.select_more_than_todays_date));
            } else {
                activityAddDealBinding.activityAddDealTvEndDate.setText(exclusiveDateFormat.format(dateTimeCalendar.getTime()));
            }
        }
    };

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if(hourOfDay<todaysHr){
            Logger.showSnackbar(AddDealActivity.this,"You can not set previous time as start time ");
        }else{
            String timeType;
            if(hourOfDay>12){
               timeType="PM";
            }else{
                timeType="AM";
            }
            hotDealHr=hourOfDay;
            hotDealMinute=minute;
            activityAddDealBinding.activityAddDealTvStartTime.setText(String.valueOf(hourOfDay)+":"+String.valueOf(minute) +" "+timeType);
        }


    }

    @Override
    public void onGetBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            switch (tag) {
                case "Iv1":
                    imageOneStatus=1;
                    activityAddDealBinding.activityAddDealIv1.setImageBitmap(bitmap);
                    path1 = imagePicker.getImagePath();
                    imageOneName=path1.substring(path1.lastIndexOf("/")+1);
                    Log.e("FileName1",imageOneName);
                    activityAddDealBinding.activityAddDealIv1Delete.setVisibility(View.VISIBLE);
                    break;
                case "Iv2":
                    imageTwoStatus=1;

                    activityAddDealBinding.activityAddDealIv2.setImageBitmap(bitmap);
                    path2 = imagePicker.getImagePath();
                    imageTwoName=path2.substring(path2.lastIndexOf("/")+1);
                    Log.e("FileName2",imageTwoName);
                    activityAddDealBinding.activityAddDealIv2Delete.setVisibility(View.VISIBLE);
                    break;
                case "Iv3":
                    imageThreeStatus=1;
                    activityAddDealBinding.activityAddDealIv3.setImageBitmap(bitmap);
                    path3 = imagePicker.getImagePath();
                    imageThreeName=path3.substring(path3.lastIndexOf("/")+1);
                    Log.e("FileName3",imageThreeName);
                    activityAddDealBinding.activityAddDealIv3Delete.setVisibility(View.VISIBLE);
                    break;

            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void openImagePicker(View view) {
        final int finePermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (finePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ;
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            imagePicker.createImageChooser();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ImagePicker.GALLERY_REQUEST) {
            uri = data.getData();
            if (!TextUtils.isEmpty(uri.toString())) {
                imagePicker.onActivityResult(requestCode, uri);
            }
        }
        if (resultCode == RESULT_OK && requestCode == ImagePicker.CAMERA_REQUEST) {
            imagePicker.onActivityResult(requestCode, uri);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.activity_add_deal_rb_hot_deal:
                activityAddDealBinding.activityAddDealHotDealView.setVisibility(View.VISIBLE);
                activityAddDealBinding.activityAddDealExclusiveDealView.setVisibility(View.GONE);
                break;
            case R.id.activity_add_deal_rb_exclusive_deal:
                activityAddDealBinding.activityAddDealHotDealView.setVisibility(View.GONE);
                activityAddDealBinding.activityAddDealExclusiveDealView.setVisibility(View.VISIBLE);
                break;
            case R.id.activity_add_deal_rb_3hrs:
                hotDealTime=3;
                break;
            case R.id.activity_add_deal_rb_4hrs:
                hotDealTime=4;
                break;
            case R.id.activity_add_deal_rb_6hrs:
                hotDealTime=6;
                break;
        }

    }
}
