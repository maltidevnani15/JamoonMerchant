package com.jamoonmerchant.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jamoonmerchant.Models.ResponseOfAllApi;
import com.jamoonmerchant.R;

import com.jamoonmerchant.databinding.ActivityLoginBinding;
import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.Logger;
import com.jamoonmerchant.utils.Utils;
import com.jamoonmerchant.webservices.RestClient;
import com.jamoonmerchant.webservices.RetrofitCallback;

import java.security.GeneralSecurityException;

import retrofit2.Call;


public class LoginActivity extends BaseActivity implements View.OnClickListener {
   private ActivityLoginBinding activityLoginBinding;


    @Override
    protected void initView() {
   activityLoginBinding= DataBindingUtil.setContentView(this,R.layout.activity_login);
        activityLoginBinding.loginTvDoLogin.setOnClickListener(this);
        activityLoginBinding.loginTvRegister.setOnClickListener(this);
        activityLoginBinding.loginEtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }

    @Override
    protected void initToolBar() {

    }
     @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_tv_doLogin:
                try {
                    validatedata();
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.login_tv_register:
                Intent i = new Intent(this,RegisterActivity.class);
                navigateToNextActivity(i,false);
                break;
        }
    }

    private void validatedata() throws GeneralSecurityException {
        final String email=activityLoginBinding.loginEtEmail.getText().toString();
        final String pwd=activityLoginBinding.loginEtPwd.getText().toString();
        if(!TextUtils.isEmpty(email)){
            if(Utils.isEmailValid(email)){
                if(!TextUtils.isEmpty(pwd)){
                    doLogin(email,pwd);
                }else {
                    Logger.showSnackbar(this,getString(R.string.enter_pwd));
                }
            }else{
                Logger.showSnackbar(this,getString(R.string.enter_email));
            }
        }else{
            activityLoginBinding.loginEtPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            Logger.showSnackbar(this,getString(R.string.enter_emaill));
        }
    }

    private void doLogin(String email, String pwd) throws GeneralSecurityException {
        final ProgressDialog dialog = Logger.showProgressDialog(this);
        final String encryptedPwd = Utils.getEncryptedPwd(pwd);
        Call<ResponseOfAllApi> loginCall= RestClient.getInstance().getApiInterface().doLogin(email,encryptedPwd);
        loginCall.enqueue(new RetrofitCallback<ResponseOfAllApi>(this,dialog) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                Logger.toast(LoginActivity.this,"Welcome");
                Utils.storeString(LoginActivity.this,Constant.USERTYPE,String.valueOf(data.getMerchantDetailModel().getMerchantType()));
                Utils.storeString(LoginActivity.this, Constant.USER_DATA, new Gson().toJson(data.getMerchantDetailModel()));
                String userData = Utils.getString(LoginActivity.this,Constant.USER_DATA);
               final Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                navigateToNextActivity(intent, false);

            }

            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                dialog.dismiss();
                Logger.toast(LoginActivity.this,error.getMessage());
            }
        });
    }

}
