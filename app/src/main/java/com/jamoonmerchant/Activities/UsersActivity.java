package com.jamoonmerchant.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jamoonmerchant.Adapters.UserListAdapter;
import com.jamoonmerchant.Interfaces.OnRecyclerItemClickListener;
import com.jamoonmerchant.Models.MerchantDetailModel;
import com.jamoonmerchant.Models.ResponseOfAllApi;
import com.jamoonmerchant.Models.UserListModel;
import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.ActivityUsersBinding;
import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.Logger;
import com.jamoonmerchant.utils.Utils;
import com.jamoonmerchant.webservices.RestClient;
import com.jamoonmerchant.webservices.RetrofitCallback;

import java.util.ArrayList;

import retrofit2.Call;

import static java.security.AccessController.getContext;

public class UsersActivity extends BaseActivity implements View.OnClickListener, OnRecyclerItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    private ActivityUsersBinding activityUsersBinding;
    private Toolbar toolbar;
    private TextView titleText;
    private UserListAdapter userListAdapter;
    private ArrayList<UserListModel> userListArrayListModel;
    private String comeFromWhere;
    private int merchantId;

    @Override
    protected void onResume() {
        super.onResume();
        userListArrayListModel.clear();
        getUserList();
    }

    @Override
    protected void initView() {
        activityUsersBinding= DataBindingUtil.setContentView(this,R.layout.activity_users);
        userListArrayListModel = new ArrayList<>();
        activityUsersBinding.activityUserSw .setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        activityUsersBinding.activityUserSw.setOnRefreshListener(this);
        activityUsersBinding.activityUserRv.setLayoutManager(new LinearLayoutManager(this));
        userListAdapter  = new UserListAdapter(this, userListArrayListModel);
        userListAdapter.setOnRecyclerItemClickListener(this);
        activityUsersBinding.activityUserRv.setAdapter(userListAdapter);
        String userData = Utils.getString(this, Constant.USER_DATA);
        if(userData!=null){
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData,MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
        }
        getUserList();
        activityUsersBinding.activityUserFb.setOnClickListener(this);
    }

    @Override
    protected void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.activity_user_tb);
        menu = (ImageView) toolbar.findViewById(R.id.row_toolbar_iv_menu);
        titleTextView = (TextView) toolbar.findViewById(R.id.row_toolbar_tv_title);
        titleText=titleTextView;
        titleText.setText("USERS");
        menu.setOnClickListener(this);
    }

    private void getUserList() {
        userListArrayListModel.clear();
        Call<ResponseOfAllApi> getuserList = RestClient.getInstance().getApiInterface().getUserList("Active",merchantId);
        getuserList.enqueue(new RetrofitCallback<ResponseOfAllApi>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if(!data.getMerchantUserListModel().isEmpty()){
                    activityUsersBinding.activityUserTvNoData.setVisibility(View.GONE);
                    userListArrayListModel.trimToSize();
                    userListArrayListModel.addAll(data.getMerchantUserListModel());
                    userListAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
                activityUsersBinding.activityUserTvNoData.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_user_fb:
                Intent i=new Intent(this,AddUserActivity.class);
                navigateToNextActivity(i,false);
                break;
        }
    }

    @Override
    public void onItemClick(int position, View view) {
        switch (view.getId()){
            case R.id.row_fragment_user_list_iv_edit:
                comeFromWhere="OnEditClick";
                Intent i=new Intent(this,AddUserActivity.class);
                final int userId= userListArrayListModel.get(position).getMerchantUserId();
                i.putExtra(comeFromWhere,comeFromWhere);
                i.putExtra("userId",userId);
               navigateToNextActivity(i,false);
                break;
        }
    }

    @Override
    public void onRefresh() {
        userListArrayListModel.clear();
        activityUsersBinding.activityUserSw.setRefreshing(false);
        getUserList();
    }

}
