package com.jamoonmerchant.Activities;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jamoonmerchant.Models.MerchantDetailModel;
import com.jamoonmerchant.Models.ResponseOfAllApi;
import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.ActivityAddUserBinding;
import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.Logger;
import com.jamoonmerchant.utils.Utils;
import com.jamoonmerchant.webservices.RestClient;
import com.jamoonmerchant.webservices.RetrofitCallback;

import java.security.GeneralSecurityException;

import retrofit2.Call;

import static java.security.AccessController.getContext;

public class AddUserActivity extends BaseActivityWithoutResideMenu implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    private ActivityAddUserBinding activityAddUserBinding;
    private int isAllow;
    private String fromWhere="DirectClick";
    private int userId;
    @Override
    protected void initView() {
        activityAddUserBinding= DataBindingUtil.setContentView(this,R.layout.activity_add_user);
            fromWhere =getIntent().getStringExtra("OnEditClick");
            userId = getIntent().getIntExtra("userId",0);
            if(userId!=0){
                getUserData(userId);
                activityAddUserBinding.activityAddUserTv.setText("UPDATE USER");
            }
        activityAddUserBinding.activityAddUserCb.setOnCheckedChangeListener(this);
        activityAddUserBinding.activityAddUserTv.setOnClickListener(this);

    }

    @Override
    protected void initToolBar() {
        final Toolbar toolbar = (Toolbar)findViewById(R.id.activity_add_user_tb);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        showBackButton(R.drawable.ic_back);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            isAllow=1;
        }else
            isAllow=0;
    }
    private void getUserData(int userId) {
        Call<ResponseOfAllApi> getUserData = RestClient.getInstance().getApiInterface().getMerchantUserData(userId);
        getUserData.enqueue(new RetrofitCallback<ResponseOfAllApi>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                activityAddUserBinding.setUserData(data.getMerchantUpdateUserData());
                activityAddUserBinding.executePendingBindings();
                setData(data.getMerchantUpdateUserData());

            }
        });
    }

    private void setData(MerchantDetailModel merchantUpdateUserData) {
        try {
            activityAddUserBinding.activityAddUserEtPwd.setText(Utils.getDecryptedPwd(merchantUpdateUserData.getPassword()));
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
    }
    private void validateData() throws GeneralSecurityException {
        final String firstName = activityAddUserBinding.activityAddUserEtFn.getText().toString();
        final String lastName = activityAddUserBinding.activityAddUserEtLn.getText().toString();
        final String password = activityAddUserBinding.activityAddUserEtPwd.getText().toString();
        final String emali = activityAddUserBinding.activityAddUserEtEmail.getText().toString();
        final String phone = activityAddUserBinding.activityAddUserEtPhone.getText().toString();

        if(!TextUtils.isEmpty(firstName)){
            if(!TextUtils.isEmpty(lastName)){
                if(!TextUtils.isEmpty(emali)){
                    if(Utils.isEmailValid(emali)){
                        if (!TextUtils.isEmpty(password)) {
                            if(!TextUtils.isEmpty(phone)){
                                if(fromWhere.equalsIgnoreCase("OnEditClick")){
                                    updateUser(firstName,lastName,emali,password,phone);
                                }else{
                                    insertUser(firstName,lastName,emali,password,phone);
                                }

                            }else{
                                Logger.showSnackbar(AddUserActivity.this,getString(R.string.enter_phone));
                            }
                        }else{
                            Logger.showSnackbar(AddUserActivity.this,getString(R.string.enter_pwd));
                        }

                    }else{
                        Logger.showSnackbar(AddUserActivity.this,getString(R.string.email_invalid));
                    }
                }else{
                    Logger.showSnackbar(AddUserActivity.this,getString(R.string.enter_email));
                }
            }else{
                Logger.showSnackbar(AddUserActivity.this,getString(R.string.enter_last_name));
            }
        }else{
            Logger.showSnackbar(AddUserActivity.this,getString(R.string.enter_first_name));
        }
    }

    private void updateUser(String firstName, String lastName, String emali, String password, String phone) throws GeneralSecurityException {
        final String encryptPwd = Utils.getEncryptedPwd(password);
        Call<JsonObject>addUser = RestClient.getInstance().getApiInterface().updateMerchantUser(userId,firstName,lastName,emali,phone,encryptPwd,isAllow);
        addUser.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")){
                   onBackPressed();
                }
                Logger.toast(AddUserActivity.this,data.get(Constant.KEY_MESSAGE).toString());
            }
        });
    }


    private void insertUser(String firstName, String lastName, String emali, String password, String phone) throws GeneralSecurityException {
        int merchantId=0;
        final String encryptPwd = Utils.getEncryptedPwd(password);
        String userData = Utils.getString(this,Constant.USER_DATA);
        if(userData!=null){
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData,MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
        }
        Call<JsonObject>addUser = RestClient.getInstance().getApiInterface().insertUser(merchantId,firstName,lastName,emali,phone,encryptPwd,isAllow);
        addUser.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")){
                  onBackPressed();
                }
                Logger.toast(AddUserActivity.this,data.get(Constant.KEY_MESSAGE).toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_add_user_tv:
                try {
                    validateData();
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
