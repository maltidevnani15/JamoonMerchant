package com.jamoonmerchant.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.jamoonmerchant.Adapters.ViewPagerAdapter;
import com.jamoonmerchant.Fragments.ActiveDealFragment;
import com.jamoonmerchant.Fragments.ExpiredDealFragment;
import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.ActivityMainBinding;

import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.Utils;
import com.special.ResideMenu.ResideMenu;

import java.util.ArrayList;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    private ActivityMainBinding activityMerchantBinding;

    private Toolbar toolbar;
    private TextView titleText;

    private static MainActivity instance;

    private int isMerchant;

    @Override
    protected void initView() {
        instance = this;
        activityMerchantBinding= DataBindingUtil.setContentView(this,R.layout.activity_main);

        String userData = Utils.getString(MainActivity.this, Constant.USER_DATA);

    }

    @Override
    protected void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.activity_main_tb);
        menu = (ImageView) toolbar.findViewById(R.id.row_toolbar_iv_menu);
        titleTextView = (TextView) toolbar.findViewById(R.id.row_toolbar_tv_title);
        titleText=titleTextView;
        titleText.setText("MYDEALS");
        menu.setOnClickListener(this);
        setUpViewPager();
    }

    public static MainActivity getinstance() {
        return instance;
    }

    private void setUpViewPager() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            final TabLayout.Tab tab = activityMerchantBinding.detailTabs.newTab();
            final View view = LayoutInflater.from(this).inflate(R.layout.layout_tab, null);
            final TextView tabTextView = (TextView) view.findViewById(R.id.tabText);
            switch (i) {
                case 0:
                    tabTextView.setText("ACTIVE");
                    tabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_active,0,0,0);
                    fragments.add(new ActiveDealFragment());
                    break;
                case 1:
                    tabTextView.setText("EXPIRED");
                    tabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_expired,0,0,0);
                    fragments.add(new ExpiredDealFragment());
                    break;



            }
            tab.setCustomView(view);
            activityMerchantBinding.detailTabs.addTab(tab);
        }

        final ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragments);
        activityMerchantBinding.viewpager.setAdapter(pagerAdapter);
        activityMerchantBinding.viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(activityMerchantBinding.detailTabs));
        activityMerchantBinding.detailTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case 0:
                        activityMerchantBinding.viewpager.setCurrentItem(tab.getPosition());
                        break;
                    case 1:
                        activityMerchantBinding.viewpager.setCurrentItem(tab.getPosition());
                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.row_toolbar_iv_menu:
                getResideMenu().openMenu(ResideMenu.DIRECTION_LEFT);
                break;
        }
    }
}
