package com.jamoonmerchant.Activities;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jamoonmerchant.Adapters.AddStoreUserListAdapter;
import com.jamoonmerchant.Adapters.LocationAdapter;
import com.jamoonmerchant.Interfaces.OnGetLocationListListener;
import com.jamoonmerchant.Interfaces.OnItemCheckChangeListener;
import com.jamoonmerchant.Models.LocationDataModel;
import com.jamoonmerchant.Models.MerchantDetailModel;
import com.jamoonmerchant.Models.ResponseOfAllApi;
import com.jamoonmerchant.Models.StoreDeatilModel;
import com.jamoonmerchant.Models.UserListModel;

import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.ActivityAddStoreBinding;
import com.jamoonmerchant.databinding.ActivityStoreBinding;
import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.GetLocations;
import com.jamoonmerchant.utils.Logger;
import com.jamoonmerchant.utils.Utils;
import com.jamoonmerchant.webservices.RestClient;
import com.jamoonmerchant.webservices.RetrofitCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;


public class AddStoreActivity extends BaseActivityWithoutResideMenu implements OnItemCheckChangeListener, OnGetLocationListListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener, View.OnClickListener {
    private ActivityAddStoreBinding activityStoreBinding;
    private LocationAdapter locationAdapterCountry;
    private LocationAdapter locationAdapterState;
    private LocationAdapter locationAdapterCity;
    private GetLocations locations;
    private int countyrId, stateId, cityId, isAllow = 0;
    private AddStoreUserListAdapter addStoreUserListAdapter;
    private ArrayList<UserListModel> userListArrayListModel;
    private StringBuffer stringBuffer;
    private ArrayList<UserListModel> assignUserListModel;
    private int storeId = 0;
    private StoreDeatilModel storeDetailModel;
    private List<String> fromUpdateStoreSelectUserListId;

    @Override
    protected void initView() {
        storeId=getIntent().getIntExtra("storeId",0);
        activityStoreBinding= DataBindingUtil.setContentView(this, R.layout.activity_add_store);
        userListArrayListModel = new ArrayList<>();
        assignUserListModel = new ArrayList<>();
        stringBuffer = new StringBuffer();
        activityStoreBinding.activityAddStoreRvUsers.setLayoutManager(new LinearLayoutManager(this));
        addStoreUserListAdapter = new AddStoreUserListAdapter(this, userListArrayListModel, this);
        addStoreUserListAdapter.setOnCheckItemListener(this);
        activityStoreBinding.activityAddStoreRvUsers.setAdapter(addStoreUserListAdapter);

        locationAdapterCountry = new LocationAdapter(this);
        locationAdapterState = new LocationAdapter(this);
        locationAdapterCity = new LocationAdapter(this);

        activityStoreBinding.activityAddStoreSpCountry.setAdapter(locationAdapterCountry);
        activityStoreBinding.activityAddStoreSpState.setAdapter(locationAdapterState);
        activityStoreBinding.activityAddStoreSpCity.setAdapter(locationAdapterCity);

        activityStoreBinding.activityAddStoreSpCountry.setOnItemSelectedListener(this);
        activityStoreBinding.activityAddStoreSpState.setOnItemSelectedListener(this);
        activityStoreBinding.activityAddStoreSpCity.setOnItemSelectedListener(this);

        locationAdapterCountry.add(getDefault("Country"));
        locationAdapterState.add(getDefault("State"));
        locationAdapterCity.add(getDefault("City"));

        locationAdapterCountry.notifyDataSetChanged();
        locationAdapterState.notifyDataSetChanged();
        locationAdapterCity.notifyDataSetChanged();

        locations = new GetLocations(this);
        locations.setListener(this);
        locations.getCountryList(GetLocations.Locations.COUNTRY, 1);

        activityStoreBinding.activityAddStoreCbAllowToManageDeal.setOnCheckedChangeListener(this);
        getUserList();
        if (storeId != 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getStoreDetailById(storeId);
                }
            }, 150);
            activityStoreBinding.activityAddStoreTvAddStore.setText("UPDATE USER");
        }
        activityStoreBinding.activityAddStoreTvAddStore.setOnClickListener(this);
    }

    @Override
    protected void initToolBar() {
        final Toolbar toolbar = (Toolbar)findViewById(R.id.activity_add_store_tb);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        showBackButton(R.drawable.ic_back);
    }
    private void getUserList() {
        String userData = Utils.getString(this, Constant.USER_DATA);
        int merchantId=0;
        if(userData!=null){
            final MerchantDetailModel merchantDetailModel=new Gson().fromJson(userData, MerchantDetailModel.class);
            merchantId=merchantDetailModel.getMerchantId();
        }
        Call<ResponseOfAllApi> getuserList = RestClient.getInstance().getApiInterface().getUserList("All", merchantId);
        getuserList.enqueue(new RetrofitCallback<ResponseOfAllApi>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if (!data.getMerchantUserListModel().isEmpty()) {
                    userListArrayListModel.trimToSize();
                    userListArrayListModel.addAll(data.getMerchantUserListModel());
                    addStoreUserListAdapter.notifyDataSetChanged();
                } else {
                  onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
                onBackPressed();
                Logger.toast(AddStoreActivity.this,"Kindly add a user first");
            }
        });
    }

    private void getStoreDetailById(int storeId) {
        Call<ResponseOfAllApi> getStoreDetailById = RestClient.getInstance().getApiInterface().getStoreDetailById(storeId);
        getStoreDetailById.enqueue(new RetrofitCallback<ResponseOfAllApi>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                storeDetailModel = data.getStoreDeatilModel();
                activityStoreBinding.setStoreData(data.getStoreDeatilModel());
                activityStoreBinding.executePendingBindings();
                activityStoreBinding.activityAddStoreSpCountry.setSelection(1);
                setSelectedUsersData();

            }
        });
    }

    private void setSelectedUsersData() {
        String str = storeDetailModel.getMerchantUserSelectListId();
        fromUpdateStoreSelectUserListId = Arrays.asList(str.split(","));
        for (int i = 0; i < userListArrayListModel.size(); i++) {
            if (fromUpdateStoreSelectUserListId.contains(String.valueOf(userListArrayListModel.get(i).getMerchantUserId()))) {
                userListArrayListModel.get(i).setIfuserSelectedForAStore(true);
                addStoreUserListAdapter.notifyDataSetChanged();

            }
        }


    }

    private LocationDataModel getDefault(String text) {
        LocationDataModel data = new LocationDataModel();
        data.setId(-1);
        data.setName(text);
        return data;
    }


    @Override
    public void onItemCheck(int position, boolean is_checked, View view) {
        if (is_checked) {
            assignUserListModel.add(userListArrayListModel.get(position));
        } else {
            assignUserListModel.remove(userListArrayListModel.get(position));
        }
    }

    @Override
    public void onLocationGetList(ArrayList<LocationDataModel> locationDataModelArrayList, GetLocations.Locations locations) {
        switch (locations) {
            case COUNTRY:
                if (locationAdapterCountry.getCount() > 0) {
                    locationAdapterCountry.clear();
                }
                if (locationAdapterState.getCount() > 0) {
                    locationAdapterState.clear();
                }
                if (locationAdapterCity.getCount() > 0) {
                    locationAdapterCity.clear();
                }
                locationAdapterCountry.add(getDefault("Country"));
                locationAdapterCountry.addAll(locationDataModelArrayList);
                locationAdapterCountry.notifyDataSetChanged();
                activityStoreBinding.activityAddStoreSpCountry.setSelection(0);

                locationAdapterState.add(getDefault("State"));
                locationAdapterState.notifyDataSetChanged();

                locationAdapterCity.add(getDefault("City"));
                locationAdapterCity.notifyDataSetChanged();

                break;
            case STATE:
                if (locationAdapterState.getCount() > 0) {
                    locationAdapterState.clear();
                }
                if (locationAdapterCity.getCount() > 0) {
                    locationAdapterCity.clear();
                }
                locationAdapterState.add(getDefault("State"));
                locationAdapterState.addAll(locationDataModelArrayList);
                locationAdapterState.notifyDataSetChanged();
                activityStoreBinding.activityAddStoreSpState.setSelection(0);

                locationAdapterCity.add(getDefault("City"));
                locationAdapterCity.notifyDataSetChanged();
                if (storeDetailModel != null) {
                    for (int j = 0; j < locationAdapterState.getCount(); j++) {
                        if (storeDetailModel.getStateId() == locationAdapterState.getItem(j).getId()) {
                            activityStoreBinding.activityAddStoreSpState.setSelection(j);
                            break;
                        }
                    }
                }
                break;
            case CITY:
                if (locationAdapterCity.getCount() > 0) {
                    locationAdapterCity.clear();
                }
                locationAdapterCity.add(getDefault("City"));
                locationAdapterCity.addAll(locationDataModelArrayList);
                locationAdapterCity.notifyDataSetChanged();
                activityStoreBinding.activityAddStoreSpCity.setSelection(0);
                if (storeDetailModel != null) {
                    for (int j = 0; j < locationAdapterCity.getCount(); j++) {
                        if (storeDetailModel.getCityID() == locationAdapterCity.getItem(j).getId()) {
                            activityStoreBinding.activityAddStoreSpCity.setSelection(j);
                            break;
                        }
                    }
                }
                break;
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            isAllow = 1;
        } else
            isAllow = 0;
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.activity_add_store_sp_country:
                countyrId = locationAdapterCountry.getItem(position).getId();
                if (countyrId != -1) {
                    locations.getStateList(GetLocations.Locations.STATE, 1, countyrId);
                }
                break;
            case R.id.activity_add_store_sp_state:
                stateId = locationAdapterState.getItem(position).getId();
                if (stateId != -1) {
                    locations.getCityList(GetLocations.Locations.CITY, 1,countyrId, stateId);
                } else {
                    if (locationAdapterCity.getCount() > 0) {
                        locationAdapterCity.clear();
                    }
                    locationAdapterCity.add(getDefault("City"));
                    activityStoreBinding.activityAddStoreSpCity.setSelection(0);
                }
                break;
            case R.id.activity_add_store_sp_city:
                cityId = locationAdapterCity.getItem(position).getId();
                Log.e("cityId", String.valueOf(cityId));
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_add_store_tv_add_store:
                validateData();
                break;

        }
    }

    private void validateData() {
        setUserId();
        final String firstName = activityStoreBinding.activityAddStoreEtStoreName.getText().toString();
        final String phone = activityStoreBinding.activityAddStoreEtPhone.getText().toString();
        final String address1 = activityStoreBinding.activityAddStoreEtAdd1.getText().toString();
        final String address2 = activityStoreBinding.activityAddStoreEtAdd2.getText().toString();
        final String zip = activityStoreBinding.activityAddStoreEtZip.getText().toString();
        if (!TextUtils.isEmpty(firstName)) {
            if (!TextUtils.isEmpty(phone)) {
                if (!TextUtils.isEmpty(address1)) {
                    if (countyrId != -1) {
                        if (stateId != -1) {
                            if (cityId != -1) {
                                if (!TextUtils.isEmpty(zip)) {
                                    if (stringBuffer.length() != 0) {
                                        if (storeId != 0) {
                                            updateStore(firstName, phone, address1, address2, zip);
                                        } else {
                                            addStoreCall(firstName, phone, address1, address2, zip);
                                        }
                                    } else {
                                        Logger.showSnackbar(AddStoreActivity.this, "Assign atleast one user to store");
                                    }

                                } else {
                                    Logger.showSnackbar(AddStoreActivity.this, "Enter zip code");
                                }
                            } else {
                                Logger.showSnackbar(AddStoreActivity.this, "Select city");
                            }
                        } else {
                            Logger.showSnackbar(AddStoreActivity.this, "Select state");
                        }
                    } else {
                        Logger.showSnackbar(AddStoreActivity.this, "Select country");
                    }
                } else {
                    Logger.showSnackbar(AddStoreActivity.this, "Enter address");
                }
            }
        } else {
            Logger.showSnackbar(AddStoreActivity.this, "Enter first name");
        }
    }

    private void updateStore(String firstName, String phone, String address1, String address2, String zip) {

        Call<JsonObject> updateStore = RestClient.getInstance().getApiInterface().storeUpdate(storeId, stringBuffer.toString(), firstName, zip, address1, address2,
                cityId, stateId, phone, isAllow);
        updateStore.enqueue(new RetrofitCallback<JsonObject>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                String message = data.get(Constant.KEY_MESSAGE).getAsString();
                if (data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")) {
                    Logger.toast(AddStoreActivity.this, message);
                    MainActivity.getinstance().onBackPressed();
                } else {
                    Logger.toast(AddStoreActivity.this, message);
                    Log.e("Message", message);
                }
            }
        });
    }

    private void addStoreCall(String firstName, String phone, String address1, String address2, String zip) {
        int merchantId = 0;
        String userData = Utils.getString(this, Constant.USER_DATA);
        if (userData != null) {
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData, MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
        }
        Call<JsonObject> addStore = RestClient.getInstance().getApiInterface().addStore(merchantId, stringBuffer.toString(), firstName, zip, address1, address2,
                cityId, stateId, phone, isAllow);
        addStore.enqueue(new RetrofitCallback<JsonObject>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                String message = data.get(Constant.KEY_MESSAGE).getAsString();
                if (data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")) {
                    Logger.toast(AddStoreActivity.this, message);
                  onBackPressed();
                } else {
                    Logger.toast(AddStoreActivity.this, message);
                    Log.e("Message", message);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    private void setUserId() {
        if (!assignUserListModel.isEmpty()) {
            for (int i = 0; i < assignUserListModel.size(); i++) {
                stringBuffer.append(assignUserListModel.get(i).getMerchantUserId());
                stringBuffer.append(",");
            }
            stringBuffer.setLength(stringBuffer.length() - 1);
        }
    }

}
