package com.jamoonmerchant.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jamoonmerchant.Adapters.StoreListAdapter;
import com.jamoonmerchant.Interfaces.OnRecyclerItemClickListener;
import com.jamoonmerchant.Models.MerchantDetailModel;
import com.jamoonmerchant.Models.ResponseOfAllApi;
import com.jamoonmerchant.Models.StoreListModel;
import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.ActivityStoreBinding;
import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.Logger;
import com.jamoonmerchant.utils.Utils;
import com.jamoonmerchant.webservices.RestClient;
import com.jamoonmerchant.webservices.RetrofitCallback;
import com.special.ResideMenu.ResideMenu;

import java.util.ArrayList;

import retrofit2.Call;

import static java.security.AccessController.getContext;

public class StoreActivity extends BaseActivity implements OnRecyclerItemClickListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    private  ActivityStoreBinding activityStoreBinding;
    private StoreListAdapter storeListAdapter;
    private ArrayList<StoreListModel> storeListModelArrayList;
    private Toolbar toolbar;
    private TextView titleText;



    @Override
    protected void initView() {
        activityStoreBinding= DataBindingUtil.setContentView(this,R.layout.activity_store);
        storeListModelArrayList = new ArrayList<>();
        activityStoreBinding.activityStoreSw .setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        activityStoreBinding.activityStoreSw.setOnRefreshListener(this);
        activityStoreBinding.activityStoreRv.setLayoutManager(new LinearLayoutManager(this));
        storeListAdapter = new StoreListAdapter(this,storeListModelArrayList);
        storeListAdapter.setOnRecyclerItemClickListener(this);
        activityStoreBinding.activityStoreRv.setAdapter(storeListAdapter);
        activityStoreBinding.activityStoreFb.setOnClickListener(this);
        getStoreList();
    }

    @Override
    protected void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.activity_store_tb);
        menu = (ImageView) toolbar.findViewById(R.id.row_toolbar_iv_menu);
        titleTextView = (TextView) toolbar.findViewById(R.id.row_toolbar_tv_title);
        titleText=titleTextView;
        titleText.setText("STORES");
        menu.setOnClickListener(this);

    }
    private void getStoreList() {

        int merchantId=0;
        String userData = Utils.getString(this, Constant.USER_DATA);
        if(userData!=null){
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData,MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
        }
        Call<ResponseOfAllApi> storeListCall = RestClient.getInstance().getApiInterface().getStoreList(merchantId);
        storeListCall.enqueue(new RetrofitCallback<ResponseOfAllApi>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                storeListModelArrayList.clear();
                if(!data.getStoreListModelArrayList().isEmpty()){
                    activityStoreBinding.activityStoreTvNoData.setVisibility(View.GONE);
                    storeListModelArrayList.trimToSize();
                    storeListModelArrayList.addAll(data.getStoreListModelArrayList());
                    storeListAdapter.notifyDataSetChanged();
                }else{
                    activityStoreBinding.activityStoreTvNoData.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
                activityStoreBinding.activityStoreTvNoData.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onItemClick(int position, View view) {
        switch (view.getId()){
            case R.id.row_fragment_store_list_iv_edit:
                Intent i = new Intent(this,AddStoreActivity.class);
                 i.putExtra("storeId",storeListModelArrayList.get(position).getStoreId());
                navigateToNextActivity(i,false);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        storeListModelArrayList.clear();
        getStoreList();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.row_toolbar_iv_menu:
                getResideMenu().openMenu(ResideMenu.DIRECTION_LEFT);
                break;
            case R.id.activity_store_fb:
                Intent i = new Intent(this,AddStoreActivity.class);
                navigateToNextActivity(i,false);
        }

    }

    @Override
    public void onRefresh() {
        storeListModelArrayList.clear();
        activityStoreBinding.activityStoreSw.setRefreshing(false);
        getStoreList();
    }
}
