package com.jamoonmerchant.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.CountDownTimer;
import android.text.TextUtils;

import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.ActivitySplashBinding;
import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.Logger;
import com.jamoonmerchant.utils.Utils;

public class SplashActivity extends BaseActivityWithoutResideMenu {
    private ActivitySplashBinding activitySplashBinding;

    @Override
    protected void initView() {
        activitySplashBinding= DataBindingUtil.setContentView(this,R.layout.activity_splash);
        new SplashCountDown(2000,1000).start();

    }

    @Override
    protected void initToolBar() {

    }


   private class SplashCountDown extends CountDownTimer{

       /**
        * @param millisInFuture    The number of millis in the future from the call
        *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
        *                          is called.
        * @param countDownInterval The interval along the way to receive
        *                          {@link #onTick(long)} callbacks.
        */
       public SplashCountDown(long millisInFuture, long countDownInterval) {
           super(millisInFuture, countDownInterval);
       }

       @Override
       public void onTick(long millisUntilFinished) {

       }

       @Override
       public void onFinish() {
           if(Utils.isConnectedToInternet(SplashActivity.this)){
               Intent intent;
               String userData = Utils.getString(SplashActivity.this,Constant.USER_DATA);
               if(userData!=null & !TextUtils.isEmpty(userData) & !userData.equalsIgnoreCase("null")){
                   intent = new Intent(SplashActivity.this, MainActivity.class);
                   navigateToNextActivity(intent, true);
               }else{
                   intent = new Intent(SplashActivity.this, LoginActivity.class);
                   navigateToNextActivity(intent, true);
               }
           }else{
               Logger.toast(SplashActivity.this,"No internet Connection");
               finish();
           }
       }
       }
}
