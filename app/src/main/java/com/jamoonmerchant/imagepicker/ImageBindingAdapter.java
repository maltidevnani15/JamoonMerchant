package com.jamoonmerchant.imagepicker;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

public class ImageBindingAdapter {
    @BindingAdapter({"bind:businessImage", "bind:error"})
    public static void loadStore(ImageView imageView,String url ,Drawable error){
        if(url !=null && !TextUtils.isEmpty(url)){
            Picasso.with(imageView.getContext()).load(url).error(error).fit().into(imageView);
        }else{
            imageView.setImageDrawable(error);
        }

    }
    @BindingAdapter({"bind:dealImageOne", "bind:error"})
    public static void loadImage1One(ImageView imageView,String url ,Drawable error){
        if(url !=null && !TextUtils.isEmpty(url)){
            Picasso.with(imageView.getContext()).load(url).error(error).fit().into(imageView);
        }else{
            imageView.setImageDrawable(error);
        }

    }
    @BindingAdapter({"bind:dealImageTwo", "bind:error"})
    public static void loadImagetwo(ImageView imageView,String url ,Drawable error){
        if(url !=null && !TextUtils.isEmpty(url)){
            Picasso.with(imageView.getContext()).load(url).error(error).fit().into(imageView);
        }else{
            imageView.setImageDrawable(error);
        }

    }
    @BindingAdapter({"bind:dealImageThree", "bind:error"})
    public static void loadImageThree(ImageView imageView,String url ,Drawable error){
        if(url !=null && !TextUtils.isEmpty(url)){
            Picasso.with(imageView.getContext()).load(url).error(error).fit().into(imageView);
        }else{
            imageView.setImageDrawable(error);
        }

    }
    @BindingAdapter({"bind:storeImage", "bind:error"})
    public static void loadStoreImage(ImageView imageView,String url ,Drawable error){
        if(url !=null && !TextUtils.isEmpty(url)){
            Picasso.with(imageView.getContext()).load(url).transform(new CircleTransform()).error(error).fit().into(imageView);
        }else{
            imageView.setImageDrawable(error);
        }

    }
}
