package com.jamoonmerchant;

import android.app.Application;

import com.jamoonmerchant.webservices.RestClient;

/**
 * Created by MaltiDevnani on 11/2/2017.
 */

public class JamoonMerchantApplication extends Application {
    JamoonMerchantApplication instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        new RestClient();
    }
}
