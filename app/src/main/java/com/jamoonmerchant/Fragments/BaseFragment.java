package com.jamoonmerchant.Fragments;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.jamoonmerchant.R;

public abstract class BaseFragment extends Fragment {
    protected abstract void initToolbar();
    protected abstract void initView(View view);




    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initToolbar();
        initView(view);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolbar();
        }
    }


}
