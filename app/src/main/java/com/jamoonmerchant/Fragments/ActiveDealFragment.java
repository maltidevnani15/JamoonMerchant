package com.jamoonmerchant.Fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.jamoonmerchant.Activities.AddDealActivity;
import com.jamoonmerchant.Activities.MainActivity;
import com.jamoonmerchant.Adapters.DealListAdapter;
import com.jamoonmerchant.Interfaces.OnRecyclerItemClickListener;
import com.jamoonmerchant.Models.DealModel;
import com.jamoonmerchant.Models.MerchantDetailModel;
import com.jamoonmerchant.Models.ResponseOfAllApi;

import com.jamoonmerchant.R;
import com.jamoonmerchant.databinding.FragmentDealsBinding;
import com.jamoonmerchant.utils.Constant;
import com.jamoonmerchant.utils.Logger;
import com.jamoonmerchant.utils.Utils;
import com.jamoonmerchant.webservices.RestClient;
import com.jamoonmerchant.webservices.RetrofitCallback;

import java.util.ArrayList;

import retrofit2.Call;


public class ActiveDealFragment extends BaseFragment implements  OnRecyclerItemClickListener, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    FragmentDealsBinding binding;
    private DealListAdapter dealListAdapter;
    private ArrayList<DealModel>dealModelArrayList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_deals, container, false);
        View view = binding.getRoot();
        return view;
    }
    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initView(View view) {
        dealModelArrayList = new ArrayList<>();
        final LinearLayoutManager manager=new LinearLayoutManager(getContext());
        dealListAdapter = new DealListAdapter(getContext(),dealModelArrayList);
        dealListAdapter.setOnRecyclerItemClickListener(this);
        binding.fragmentDealSwipeLl .setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        binding.fragmentDealSwipeLl.setOnRefreshListener(this);
        binding.fragmentActiveDealRv.setLayoutManager(manager);
        binding.fragmentActiveDealRv.setAdapter(dealListAdapter);
        binding.fragmentActiveDealFb.setOnClickListener(this);
        getDealList();

    }

    private void getDealList() {
        dealModelArrayList.clear();
       int merchantId=0;
         int merchanType=0;
         int merchantUserId=0;
        String userData = Utils.getString(getContext(), Constant.USER_DATA);
        if (userData != null) {
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData, MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
            merchanType = Integer.parseInt(Utils.getString(getContext(),Constant.USERTYPE));
            merchantUserId = merchantDetailModel.getMerchantUserId();
        }
        Call<ResponseOfAllApi>getDeal = RestClient.getInstance().getApiInterface().getDeals(merchanType,merchantId,merchantUserId);
        getDeal.enqueue(new RetrofitCallback<ResponseOfAllApi>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if(!data.getDealModelArrayList().isEmpty()){
                    binding.fragmentDealTvNodata.setVisibility(View.GONE);
                    dealModelArrayList.trimToSize();
                    dealModelArrayList.addAll(data.getDealModelArrayList());
                    dealListAdapter.notifyDataSetChanged();
                }
                else {
                    binding.fragmentDealTvNodata.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
                binding.fragmentDealTvNodata.setVisibility(View.VISIBLE);
            }
        });
    }



    @Override
    public void onItemClick(int position, View view) {
        int dealId = dealModelArrayList.get(position).getDealId();
       final Intent i=new Intent(getActivity(), AddDealActivity.class);
       i.putExtra("dealId",dealId);
        i.putExtra("FromEdit","fromEdit");
        startActivity(i);


    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        getDealList();
    }

    @Override
    public void onRefresh() {
        getDealList();
        binding.fragmentDealSwipeLl.setRefreshing(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_active_deal_fb:
                Intent i = new Intent(getActivity(),AddDealActivity.class);
                startActivity(i);
                break;
        }

    }
}
